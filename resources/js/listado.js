window.onload  = () =>{
    setTimeout(() => {
        var div = document.getElementById('divok')
        if(div!=null){
            div.remove()
        }
    }, 3000);
}

let btnEliminar = document.querySelector('#btnEliminar');

window.datos = (id,nombre) =>{
    let lbl_nombre = document.getElementById('lbl_nombre');
    lbl_nombre.innerHTML = nombre;
    btnEliminar.setAttribute('data-id',id);
}

btnEliminar.addEventListener('click',() =>{
    let id = btnEliminar.getAttribute('data-id');
    let form = document.querySelector('#frm_'+id);
    form.submit();
});