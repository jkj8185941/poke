@extends('plantilla')
@section('titulo')
- @yield('form')
@endsection
@section('principal')
@if ($errors->any())
<div class="row">
  <div class="col-md-6 offset-md-3">
    <div class="alert alert-danger">
      <ul>
      @foreach ($errors->all() as $e)
        <li>{{$e}}</li>  
      @endforeach
      </ul>
    </div>
  </div>
</div>
@endif
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">@yield('form')</div>
            <div class="card-body">
                    <form method="POST" @yield('action')>
                        @csrf
                        @yield('metodo')
                        <div class="input-group mb-3">
                            <span class="input-group-text">@</span>
                            <input name="nombre" type="text" class="form-control" placeholder="Nombre" @isset($pokemon) value="{{$pokemon->nombre}}"@endisset>
                          </div>
                          <div class="input-group mb-3">
                            <span class="input-group-text">@</span>
                            <input name="tipo" type="text" class="form-control" placeholder="Tipo" @isset($pokemon) value="{{$pokemon->tipo}}"@endisset>
                          </div>
                          <div class="input-group mb-3">
                            <span class="input-group-text">@</span>
                            <input name="habilidades" type="text" class="form-control" placeholder="Habilidades" @isset($pokemon) value="{{$pokemon->habilidades}}"@endisset>
                          </div>
                          <div class="input-group mb-3">
                            <span class="input-group-text">@</span>
                            <input name="pokedex" type="text" class="form-control" placeholder="Pokedex" @isset($pokemon) value="{{$pokemon->pokedex}}"@endisset>
                          </div>
                          <button class="btn btn-info">Guardar</button>                         
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection