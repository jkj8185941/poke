<?php

namespace App\Http\Controllers;

use App\Models\Pokemon;
use Illuminate\Http\Request;

class PokemonController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pokemones = Pokemon::all();
        return view('pokevista',compact('pokemones'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('crear');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
     $request->validate([
        'nombre'=> 'required',
        'tipo'=> 'required',
        'habilidades'=> 'required',
        'pokedex'=> 'required|numeric',
     ]);
     $pokemon = new Pokemon($request->input());
     $pokemon -> save();
     return redirect()->route('pokemones.index')
     ->with('success','Pokemon guardado');////////
    }

    /**
     * Display the specified resource.
     */
    public function show(Pokemon $pokemon)
    {
        

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $pokemon = Pokemon::find($id);
        return view('editar',compact('pokemon'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre'=> 'required',
            'tipo'=> 'required',
            'habilidades'=> 'required',
            'pokedex'=> 'required|numeric',
         ]);
        $pokemon = Pokemon::find($id);////
        $pokemon -> update($request->input());
        return redirect()->route('pokemones.index')
        ->with('success','Pokemon actualizado');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $pokemon = Pokemon::find($id);///
        $pokemon -> delete();
        return redirect()->route('pokemones.index')
        ->with('success','Pokemon eliminado');
    }
}
